# dijangogirls 


### Instalação para Desenvolvimento

1. Clone o projeto, crie o ambiente virtual e faça a instalação das dependências (dev).
É necessário ter `python 3.8.2-alpine` e `virtualenv`.  

    ```sh
    $ git clone git@gitlab.com:felixcouto/dijangogirls.git
    $ cd dijangogirls
    $ virtualenv env -p python3.6
    $ env/bin/pip install -r requirements-dev.txt
    ```
2. Crie o arquivo de configuração local `dijangogirls/local_settings.py`, baseado no arquivo `dijangogirls/local_settings.py.example`. Não apague o `dijangogirls/local_settings.py.example`. 

3. Faça o migrate, crie o superuser e execute o projeto.

    ```sh
    $ env/bin/python manage.py makemigrations
    $ env/bin/python manage.py migrate
    $ env/bin/python manage.py createsuperuser
    $ env/bin/python manage.py runserver
    ```
